package com.example.kotlin.taskmanager.service

import com.example.kotlin.taskmanager.Process

class TaskManagerFifo: TaskManager {

    constructor(initialSize: Int = 10): super(initialSize, compareBy<Process>({ it.timestamp}))

    override fun handleFullQueue(process: Process): Boolean {
        val processToRemove = super.processQueue.peek()
        return super.killById(processToRemove.id)
    }
}