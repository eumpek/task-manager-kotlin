package com.example.kotlin.taskmanager.service

import com.example.kotlin.taskmanager.Process
import java.util.concurrent.PriorityBlockingQueue
import kotlin.Comparator
import kotlin.collections.HashMap
import kotlin.collections.HashSet


open class TaskManager(initialSize: Int = 10, comparator: Comparator<Process> = compareBy { it.priority }) {

    protected var maxSize: Int = initialSize
    protected val processQueue: PriorityBlockingQueue<Process> = PriorityBlockingQueue(initialSize, comparator)
    protected val processById: HashMap<Int, Process> = HashMap()
    protected val processesByPriority: HashMap<Int, HashSet<Int>> = HashMap()

    open protected fun handleFullQueue(process: Process): Boolean {
        throw IllegalStateException("Queue is full, process $process.id has not been added")
    }

    /**
     * Synchronously adds processes to the priority queue
     * Returns true if the process has been successfully added
     */
    @Synchronized fun add(process: Process): Boolean {

        if (processQueue.size == this.maxSize && !handleFullQueue(process)) {
           return false
        }

        processById[process.id] = process
        val processIdsByPriority: HashSet<Int> = processesByPriority.getOrDefault(process.priority, HashSet())
        processIdsByPriority.add(process.id)
        processesByPriority[process.priority] = processIdsByPriority

        processQueue.add(process)
        return true
    }

    /**
     * Returns the running processes as List
     * sorted by creation timestamp
     */
    fun list(): List<Process> {
        return processById.values.sortedBy { it.timestamp }
    }

    /**
     * Returns the true if the process of the give [id] has been killed otherwise false
     */

    fun killById(id: Int): Boolean {
        if (!processById.containsKey(id)) return false

        val process = processById[id] as Process
        val processIdsByPriority = processesByPriority[process.priority] as HashSet<Int>
        processById.remove(id)
        processIdsByPriority.remove(process.id)
        process.kill()
        return processQueue.remove(process)
    }

    /**
     * Returns the true if at least one process was killed otherwise false
     */
    fun killByPriority(priority: Int): Boolean {
        if(!processesByPriority.containsKey(priority) || processesByPriority[priority]?.isEmpty() == true)
             return false

        val processIds: MutableSet<Int> = processesByPriority[priority]!!.toMutableSet()
        for (processId in processIds ) {
            this.killById(processId)
        }
        return true
    }

    fun killAll(): Boolean {

        if(processById.isEmpty())
            return false

        for (process in processById.toMutableMap().values) {
            this.killById(process.id)
        }
        return true
    }

}