package com.example.kotlin.taskmanager.service

import com.example.kotlin.taskmanager.Process
import java.util.Collections

class TaskManagerPriorityBased: TaskManager {

    constructor(initialSize: Int): super(initialSize, compareBy<Process>({it.priority}, { it.timestamp}))

    override fun handleFullQueue(process: Process): Boolean {
        val priorities = super.processesByPriority.keys as Set<Int>
        val maxPriority : Int = Collections.max(priorities) as Int

        if(process.priority <= maxPriority) return false
        val processToRemove = super.processQueue.peek()
        return super.killById(processToRemove.id)
    }
}