package com.example.kotlin.taskmanager

import java.util.Objects


data class Process(val id: Int, val priority: Int, val timestamp: Long) {
    override fun equals(other: Any?): Boolean {
        return (other is Process)
                && id == other.id
    }

    override fun hashCode(): Int {
        return Objects.hashCode(id)
    }

    fun kill() {
        println("Process #$id has been killed")
    }
}
