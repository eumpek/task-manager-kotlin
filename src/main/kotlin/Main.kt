import com.example.kotlin.taskmanager.Process
import com.example.kotlin.taskmanager.service.TaskManager
import com.example.kotlin.taskmanager.service.TaskManagerFifo
import com.example.kotlin.taskmanager.service.TaskManagerPriorityBased

fun main() {


//    val tm = TaskManager(5)
//    val tm = TaskManagerFifo(5)
    val tm = TaskManagerPriorityBased(3)


    tm.add(com.example.kotlin.taskmanager.Process(1, 1, 98))
    tm.add(com.example.kotlin.taskmanager.Process(2, 2, 99))
    tm.add(com.example.kotlin.taskmanager.Process(3, 1, 101))
    tm.add(com.example.kotlin.taskmanager.Process(4, 1, 102))
    tm.add(com.example.kotlin.taskmanager.Process(5, 0, 103))
    tm.add(com.example.kotlin.taskmanager.Process(6, 3, 104))
    tm.add(com.example.kotlin.taskmanager.Process(7, 3, 105))

    for(p in tm.list()){
        println(p)
    }

}
