package com.example.kotlin.taskmanager.service

import com.example.kotlin.taskmanager.*
import org.junit.jupiter.api.Assertions.assertArrayEquals
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

internal class TaskManagerTest {

    private val tm = TaskManager(5)
    @BeforeEach
    fun init() {
        tm.add(Process(1, 1, 100))
        tm.add(Process(2, 2, 101))
        tm.add(Process(3, 2, 102))
        tm.add(Process(4, 2, 103))
    }

    @Test
    fun add() {
        assertTrue(tm.add(Process(5, 3, 104)), "Add a new process when there's enough space")
        assertEquals(tm.list().size, 5, "List size increased after the addition of a new process")

        assertEquals(tm.list().indexOf(Process(5, 3, 104)), 4,"Sorts processes based on timestamp")

        assertThrows <IllegalStateException> {
            assertTrue(tm.add(Process(6, 3, 105)), "Fails to insert new process when the list is full")
        }
    }

    @Test
    fun list() {
        val actualProcessArray: Array<Process> = tm.list().toTypedArray()
        val expectedProcessArray = arrayOf(
            Process(1, 1, 100),
            Process(2, 2, 101),
            Process(3, 2, 102),
            Process(4, 2, 103)
        )
        assertArrayEquals(actualProcessArray,expectedProcessArray, "Lists of processes contains all processes added")

    }

    @Test
    fun killById() {
        assertFalse(tm.killById(5),"Fails to kill not found processes")
        assertEquals(tm.list().toTypedArray().size, 4 , "Kills processes with given id")

        assertTrue(tm.killById(3))

        val actualProcessArray: Array<Process> = tm.list().toTypedArray()
        val expectedProcessArray = arrayOf(
            Process(1, 1, 100),
            Process(2, 2, 101),
            Process(4, 2, 103)
        )

        assertArrayEquals(actualProcessArray,expectedProcessArray, "Lists the remaining processes after deletion")
    }

    @Test
    fun killByPriority() {
        assertFalse(tm.killByPriority(3), "Fails to kill not found processes")
        assertTrue(tm.killByPriority(2), "Kills processes with given priority")

        val actualProcessArray: Array<Process> = tm.list().toTypedArray()
        val expectedProcessArray = arrayOf(
            Process(1, 1, 100)
        )

        assertArrayEquals(actualProcessArray,expectedProcessArray, "Lists the remaining processes after deletion" )
    }

    @Test
    fun killAll() {
        assertTrue(tm.killAll(), "Kills all processes")
        assertEquals(tm.list().size, 0 , "Process list is empty")
    }
}