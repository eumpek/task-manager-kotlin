package com.example.kotlin.taskmanager.service

import com.example.kotlin.taskmanager.Process
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

internal class TaskManagerPriorityBasedTest {

    private val tm = TaskManagerPriorityBased(5)

    @BeforeEach
    fun init() {
        tm.add(Process(1, 2, 100))
        tm.add(Process(2, 2, 101))
        tm.add(Process(3, 1, 102))
        tm.add(Process(4, 1, 103))
    }

    @Test
    fun add() {
        assertTrue(tm.add(Process(5, 2, 104)), "Add a new process when there's enough space")
        assertEquals(tm.list().size, 5, "List size increased after the addition of a new process")

        assertEquals(Process(5, 2, 104), tm.list()[4], "Inserted Process is the last one in the array")

        assertFalse(tm.add(Process(6, 2, 105)), "Skips process since it's priority it's not bigger than the rest")

        assertTrue(tm.add(Process(7, 3, 106)), "Add process since it's priority is bigger than the rest")

        assertEquals(
            tm.list().indexOf(Process(3, 1, 102)),
            -1,
            "New process kicked out the oldest with lowest priority"
        )

    }
}