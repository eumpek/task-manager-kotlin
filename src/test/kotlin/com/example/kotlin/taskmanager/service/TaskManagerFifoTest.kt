package com.example.kotlin.taskmanager.service

import com.example.kotlin.taskmanager.Process
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

internal class TaskManagerFifoTest {

    private val tm = TaskManagerFifo(5)


    @BeforeEach
    fun init() {
        tm.add(Process(1, 1, 100))
        tm.add(Process(2, 2, 101))
        tm.add(Process(3, 2, 102))
        tm.add(Process(4, 2, 103))
    }

    @Test
    fun add() {
        assertTrue(tm.add(Process(5, 3, 104)))
        assertEquals(tm.list().size, 5)

        assertEquals(Process(5, 3, 104), tm.list()[4], "Inserted Process is the last one in the array")

        assertTrue(tm.add(Process(6, 3, 105)), "Add process by kicking out the oldest one")

        assertEquals(Process(6, 3, 105), tm.list()[4], "Inserted Process is the last one in the array")

        assertEquals(Process(2, 2, 101), tm.list()[0], "The process with id 1 (oldest) has been kicked out")

    }
}